package b137.alinabon.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args){
        System.out.println("Leap Year Calculator");

        Scanner appScanner = new Scanner(System.in);

        //System.out.println("What is your firstname?\n ");
        //String firstName = appScanner.nextLine();

        //System.out.println("Hello, " + firstName + "!\n");


        // Activity: Create a program that check if a year is a leap year or not.

        System.out.println("Enter a year: ");
        int year = appScanner.nextInt();

        if((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))){
            System.out.println(year + " is a leap year");
        }else{
            System.out.println(year + " is not a leap year");
        }
    }
}
